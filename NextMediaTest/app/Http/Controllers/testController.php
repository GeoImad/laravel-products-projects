<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;
use App\Models\category;

class testController extends Controller
{
    //
    public function show()    // to show data
    {
        $sel = \DB::table('product')->get();
        $sel2 = product::all();
        
        
        $sel3 = category::all();
        
        return view('home')->with(["prods" => $sel2, "categs" => $sel3]);
    }

    public function add(Request $req)       // to create new one
    {
        \DB::table('product')->insert(                          // insert data
            ['name' => $req->prodName, 'description' => $req->prodDesc, 'price' => $req->prodPrice]
        );

        
        $sel2 = product::all();
        
        $sel3 = category::all();
        
       
    
        
        return view('home')->with(["prods" => $sel2, "categs" => $sel3]);
    }

    public function sortName()      // Sort by name
    {
        
        $sel2 = product::orderBy('name', 'ASC')->get();
        $sel3 = category::all();
        
        return view('home')->with(["prods" => $sel2, "categs" => $sel3]);
    }

    public function sortPrice()      // Sort by Price
    {
        
        $sel2 = product::orderBy('price', 'ASC')->get();
        $sel3 = category::all();
        
        return view('home')->with(["prods" => $sel2, "categs" => $sel3]);
    }
    
}
